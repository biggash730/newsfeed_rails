class CreateFeedNames < ActiveRecord::Migration
  def change
    create_table :feed_names do |t|
      t.string :name
      t.string :url

      t.timestamps
    end
  end
end
