# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

FeedName.create!(name: 'BBC News ', url: 'http://feeds.bbci.co.uk/news/rss.xml')
FeedName.create!(name: 'BBC World ', url: 'http://feeds.bbci.co.uk/news/world/rss.xml')
FeedName.create!(name: 'BBC Africa ', url: 'http://feeds.bbci.co.uk/news/world/africa/rss.xml')
FeedName.create!(name: 'BBC Technology ', url: 'http://feeds.bbci.co.uk/news/technology/rss.xml')
FeedName.create!(name: 'CNN News ', url: 'http://rss.cnn.com/rss/edition.rss')
FeedName.create!(name: 'CNN World ', url: 'http://rss.cnn.com/rss/edition_world.rss')
FeedName.create!(name: 'CNN Africa ', url: 'http://rss.cnn.com/rss/edition_africa.rss')
FeedName.create!(name: 'CNN Technology ', url: 'http://rss.cnn.com/rss/edition_technology.rss')
FeedName.create!(name: 'My Joy Oline ', url: 'http://www.medpagetoday.com/rss/Headlines.xml')
FeedName.create!(name: 'Ghana Web ', url: 'http://www.ghanaweb.com/feed/newsfeed.xml')