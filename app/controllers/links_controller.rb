class LinksController < ApplicationController
  respond_to :json
  
  def index
    res = FeedName.where('name like ?', "%#{params[:q]}%").map { |r| {id: r.id, text: r.name, url: r.url} }
    respond_with res
  end
end