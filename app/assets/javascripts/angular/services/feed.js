newsfeed.service("FeedServices",function( $http, $q ) {
  this.getFeeds = function(feedUrl) {

    $http({
      method: "Get",
      url: feedUrl,
      cache: true,
      dataType: 'jsonp',
      params: {},
      crossDomain: true,
      headers: {'Access-Control-Allow-Origin': '*'}
    }).success(function(data, status){
      console.log("Status: "+status + " Data: "+data);
      return data;
    })
    .error(function(data, status, headers, config){
      console.log("Status: "+status + " Data: "+data);
      return [];
    });
  }

  this.getTest = function(feedUrl) {

    var responsePromise = $http.get(feedUrl);

    responsePromise.success(function(data, status, headers, config) {
      console.log(data);
      return true;
    });
    responsePromise.error(function(data, status, headers, config) {
      console.log(data);
      return false;
    });
  }

  this.parseFeed = function(url){
      return $http.jsonp('//ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=50&callback=JSON_CALLBACK&q=' + encodeURIComponent(url));
    }

});