newsfeed.service('LinksService', ['$http', function ($http) {

        var urlBase = '/api/links';

        this.getLinks = function () {
            return $http.get(urlBase+'.json');
        };

        this.getLink = function (id) {
            return $http.get(urlBase + '/' + id);
        };

        this.insertLink = function (link) {
            return $http.post(urlBase, link);
        };

        this.updateLink = function (link) {
            return $http.put(urlBase + '/' + link.Id, link)
        };

        this.deleteLink = function (id) {
            return $http.delete(urlBase + '/' + id);
        };
    }]);