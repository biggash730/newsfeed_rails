newsfeed.controller('NewsCtrl', ['$scope', 'FeedServices','$http', '$q',function($scope, FeedServices, $http, $q) {
	$scope.showOneFeed = false;
	$scope.oneFeed = {};
	
	$scope.$watch('feedName', function() {
		if ($scope.feedName != "" && $scope.feedName != null) {
			
			var feeds = FeedServices.parseFeed($scope.feedName.url).then(function(res){
				$scope.feeds=res.data.responseData.feed.entries;
				$scope.feedTitle =  res.data.responseData.feed.title;
				$scope.feedDesc =  res.data.responseData.feed.description;
			});
		}
	});

	$scope.select2Options = {
		ajax: {
			url: "https://dl.dropboxusercontent.com/u/90572675/links.js",
			dataType: 'json',
			data: function(term, page) {
				return {};
			},
			results: function(data, page) {
      			//console.log(data[0]);
      			return {
      				results: data
      			};
      		}
      	}
      };

      $scope.showFeed = function(feed) {
      	$scope.showOneFeed = true;
      	console.log(feed);
      };
  }]);